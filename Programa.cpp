/*
 * g++ MainPila.cpp Pila.cpp -o Pila
 * 
 * o ejecutar:
 * 
 * make
 */
#include <iostream>
using namespace std;
// Se llama al archivo pila.cpp 
#include "Pila.cpp"

// Se llama a la funcion que contiene el menu 
int opciones_menu(){
	// se solicita al usuario ingresar su opcion
    int opcion;
    // Se muestra la informacion en pantalla 
    cout << " INFORMACION RESPECTIVO PILA" << endl;
    // Se muestra ademas el menu de opciones 
    cout << " MENU DE OPCIONES \n" << endl;
    cout << " OPCION 1, AGREGAR/PUSH \n" << endl;
    cout << " OPCION 2, REMOVER/POP \n" << endl;
    cout << " OPCION 3, VER PILA \n" << endl;
    cout << " OPCION 0, SALIR DEL PROGRAMA \n" << endl;

    // Se  solicita al usuario ingresar su opcion 
    cout << "Ingrese su opcion:" << endl;
    cin >> opcion;
    return opcion;
}

// Se define el menu principal
int main(){
    // Se solicita el dato y opcion
    int dato;
    int opcion;
    // Se llama al objeto y uno nuevo, el tamaño maximo es 6, este se determina
    Pila pila = Pila(6);
    opcion = opciones_menu();
    // Si el usuario elige la PRIMERA opcion sucedera lo siguiente
    while(opcion != 0){
        switch (opcion){
            case 1:
            // SE PIDE INGRESAR EL VALOR
            cout << "INGRESANDO VALOR" << endl;
            cin >> dato;
            // AGREGR LO QUE SE PASA Y EL VALOR 
            pila.Pila_push(dato);
            // Se piden las opciones de nuevo
            opcion = opciones_menu();
            break;
 
    // Si el usuario elige la SEGUNDA opcion sucedera lo siguiente
            case 2:
            // Se remueve el dato
            pila.Pila_pop();
            // Se piden las opciones de nuevo
            opcion = opciones_menu();
            break;

    // Si el usuario elige la TERCERA opcion sucedera lo siguiente
            case 3:
            cout << "IMPRIMIENDO VALOR" << endl;
            // AGREGAR LA FUNCION QUE PERMITA AGREGAR LA PILA
            pila.imprimir_arreglo();
            // Se piden las opciones de nuevo
            opcion = opciones_menu();
            break;
    // Si el usuario elige la CUARTA opcion sucedera lo siguiente
case 0:
            // El programa se cierra
            cout << "CERRANDO PROGRAMA";
            exit(0);
        }
      }
        system("pause");
      return 0;
}