/*
 * g++ Pila.cpp -o Pila
 * 
 * o ejecutar:
 * 
 * make
 */
 #include <iostream>
using namespace std;
// Se llama al archivo cabecera del programa
#include "Pila.h"

// Se llama a la clase pila y el constructor
Pila::Pila(){}

// Se llama a la clase pila y los atributos principales que producen el funcionamiento del programa
Pila::Pila(int maximo) {
	// Esta el atributo del maximo y la pila 
    this-> maximo = maximo - 1;
    this-> pila = new int[maximo];
}

// Se crea la funcion de la pila vacia 
void Pila::Pila_vacia() { 
	//Esta funcion se encarga de verificar que la pila este vacia
	// Si el tope es igual a -1
    if (this->tope == -1){
		//la bandera sera true, La pila ESTA llena
        this->band = true;
	// Se modo contrario si no alcanza el tope
	}else{
		// la bandera sera falsa, La pila NO esta vacia
        this->band = false;
	}
}

// se crea la funcion de la pila llena
void Pila::Pila_llena() { 
	// Verifica que la pila este llena , el tope debe ser igual al maximo
    if (this->tope == this->maximo){
    // la bandera es verdadero, por lo tanto la pila ESTA llena
        this->band = true;
    }
    else{
		// Si la bandera no es igual al maximo, la pila NO esta llena
        this->band = false;
	}
}

// Se crea la funcion que accede al dato en la posicion i 
int Pila::get_dato(int i) {
	// Se retorna el valor de la pila en la posicion
    return this->pila[i];
}

// se crea la funcion de la pila que agrega
void Pila::Pila_push(int dato) {
	// se llama a la funcion que verifica si la pila esta llena 
    this-> Pila_llena();
    // si la bandera es verdadero la pila esta llena  
    if (this->band == true) {
		// Se imprime si la pila se desborda y no es posible agregar mas 
        cout << " Desbordamiento, pila llena " << endl;
    } 
    else {
		// De modo contrario aumenta el tope y se agrega el dato
        this->tope++;
        this->pila[this->tope] = dato;
    }
}

// Se crea la funcion pila_pop la cual remueve datos si la pila no esta llena 
void Pila::Pila_pop() {
	// se llama al valor del dato y a la funcion que verifica si la pila esta vacia
    int dato;
    this-> Pila_vacia();
	// si la bandera es true  la pila esta vacia
    if (this->band == true) {
		// Se imprime el mensaje que la pila esta vacia
        cout << "Subdesbordamiento, pila vacia" << endl;
    }
    else {
		// De modo contrario se elimina la el dato que esta en la pila 
        dato = this->pila[this->tope];
        //this->pila[this->tope] = " ";
        this->tope--;
        cout << "EL DATO ELIMINADO ES: " << dato << endl;
    }
}

// Se imprime la funcion del arreglo 
void Pila::imprimir_arreglo(){
	// Se imprime de la siguiente manera el arreglo, con -- para mostrar la pila
	for(int i=tope; i>=0 ; i --){
		// Se imprimen los datos con un espacio en blanco
		cout << " " << pila[i] << " ";
	}
	cout << endl;
}
