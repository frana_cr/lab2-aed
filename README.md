# Pilas y Colas Circulares con Arreglos
## Comenzando
El proposito de este programa es poder implementar elementos para crear y mostar una pila-cola, se debe utilizar las clases y tambien arreglos para la construccion del programa, tambien tendra un menu principal el cual permite la interaccion con el usuario para que ingrese su opcion preferente y luego se realice la funcion escogida. 
El programa funciona mendiante un archivo en formato .h en este caso el archivo Pila.h es el archivo de cabecera el cual permite otorgar el funcionamiento de la clase Pila y los atributos, se genera un constructor y se le otrogan determinados parametros lo que producira el funcionamiento de la clase en el programa. Tambien en este archivo se detallan las funciones principales que llevan a cabo la ejecucion del codigo. 
- El archivo Pila.cpp es esta escrito en lenguaje C++ es el encargado del funcionamiento de las clases y tambien de definir los parametros y atributos las clases principales que se crearon son: 
- Llamado del constructor y de la funcion que genera la que el codigo funcione de mejor manera.
- Pila_vacia(): esta funcion estara encargada de ver si la pila esta llena o vacia segun las condiicones que sea el tope, para poder conocer el estado la bandera entregara un vaor booleano true o false, lo mismo sucede con la funcion de Pila_llena(), pero la diferencia es que compara el valor del tope con el valor del maximo que en este caso es el valor 6, estas funciones son basicas e importantes para poder llevar a cabo si se agrega o elimina un dato del arreglo.
- La funcion get_dato(): permite obtener el dato y la posicion que este tendra dentro de la pila.  
- Pila_push(): esta funcion es la encargada de poder agregar un dato que ingrese el usuario para esto llama a ala funcion Pila_llena, si esta esta llena arroja un mensaje al usuaio, si esta condicion no se cumple es posible seguir agregando datos.
- Pila_pop(): esta funcion es la encargada de eliminar un dato del arreglo , esto se puede revisar por medio de la funcion Pila_vacia(), la cual si tiene una pila vacia indica al usuario que no se puede seguir borrando porque no hay mas datos en el arreglo, si la bandera tiene el boolenano false significa que mostrara el dato que ha sido eliminado sin problemas.
- Funcion imprimir_arreglo(): esta funcion permite imprimir el arreglo, segun lo que se quiere mostarra se puede manipular el for en funcion del tope o el valor de la posicion, con esta funcion podemos obtener el arreglo con los datos que se ingresan y se remueven, tambien permite conocer el estado de la pila. 

En el archivo Programa.cpp se crean dos funciones principales en la primera se detallan las opciones que se mostraran en pantalla al usuario donde se pueden agregar datos, quitar un dato, mostrar todo el arreglo y tambien cerrar el programa, el usuario por medio de la opcion que ingrese se llamara al objeto. 
Luego en la funcion principal main se solicitan a los datos y las opciones ambas ingresados por el usuario, tambien se crea el objeto pila y el objeto que se implementara para su modificacion, el maximo del arreglo esta determinado por el autor del programa y no es solicitado al usuario. Si el usuario ingresa la primera opcion se agregara un dato al programa el cual es alamacenado en el arreglo, luego tambien se ingresa la segunda opcion se eliminara un valor del arreglo esto esta determinado por las funciones creadas en el archivo Pila.cpp, por ultimo estan las opcioens de imprimir los datos del arreglo y tambien la opcion de cerrar el programa si se desea. 
## Prerequisitos
- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (vim o geany)

### Instalacion
Para poder ejecutar el programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:
lsb_release -a (versión de Ubuntu)
En donde:
Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando.
Para descargar un editor de texto como vim se puede descargar de la siguiente manera:
sudo apt install vim
En donde: Por medio de este editor de texto se puede construir el codigo.
En el caso del desarrollo del trabajo se implemento el editor de texto Geany, descargado de Ubuntu Software.

### Ejecutando Pruebas
- Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo. o por medio de un archivo Makefile donde se reunen las condiciones para hacer funcionar el programa

## Construido con
Ubuntu: Sistema operativo.
C++: Lenguaje de programación.
Geany: Editor de código.

## Versiones
Versiones de herramientas:
Ubuntu 20.04 LTS
Geany 1.36-1build1
Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/lab2-aed

## Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.

## Expresiones de gratitud
A los ejemplos en la plataforma de Educandus de Alejandro Valdes: https://lms.educandus.cl/mod/lesson/view.php?id=730534&pageid=13730, al ayudante del modulo se otorgan las gracias por solucionar dudas y ayudar a comprender errores. 


