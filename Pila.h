#include <iostream>
using namespace std;
// Se crea el archivo en el formato.h, el cual es la cabecera del trabajo 
#ifndef PILA_H
#define PILA_H
// Se define la clase principal pila con sus atributos publicos y privados
class Pila {
	// se definen los atributos privados
    private:
        int maximo = 0;
        int tope = -1;
        int *pila = NULL;
        bool band = false;
    // Se definen los atributos publicos
    public:
    // Se define el constructor
        Pila();
        // Al constructor se le otroga el atributo del maximo
        Pila(int maximo);
        // Se crean las funciones que llevan a cabo el programa 
        void Pila_vacia();
        void Pila_llena();
        int get_dato(int i);
        // Esta funcion inserta
        void Pila_push(int dato);
        // Esta funcion elimina
        void Pila_pop();
        // Esta funcion imprime el arreglo
        void imprimir_arreglo();
};
#endif